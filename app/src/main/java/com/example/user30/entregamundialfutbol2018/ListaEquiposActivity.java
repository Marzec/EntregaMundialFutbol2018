package com.example.user30.entregamundialfutbol2018;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.user30.entregamundialfutbol2018.databinding.ActivityListaEquiposBinding;


public class ListaEquiposActivity extends AppCompatActivity{

        private ListaEquiposController controller;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            String equipoInicial = (String) this.getIntent().getSerializableExtra("teams");

            controller = new ListaEquiposController(this, equipoInicial);
            ActivityListaEquiposBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_lista_equipos);
            binding.setController(this.controller);

            this.agregarOnClick();
        }

        private void agregarOnClick() {
            ListView listaDeEquipos = this.findViewById(R.id.listaDeEquipos);

            listaDeEquipos.setOnItemClickListener((listView, viewFila, position, elIdDeNoSeQue) -> {
                this.controller.mostrarEquipo(position);
            });
        }

    }
