package com.example.user30.entregamundialfutbol2018;


import android.databinding.BaseObservable;
import android.support.v7.app.AppCompatActivity;

import com.example.user30.entregamundialfutbol2018.model.Estadio;

public class EstadioActivityController extends BaseObservable {

    private AppCompatActivity activity;
    private Estadio estadioAmostrar;

    public EstadioActivityController(AppCompatActivity _activity) {
        this.activity = _activity;
    }

    public String getCiudad() {
        return this.estadioAmostrar.getCiudad();
    }
    public String getCapacidad(){
        return this.estadioAmostrar.getCapacidad();
    }
    public String getNombreEstadio(){
        return this.estadioAmostrar.getNombre();
    }
    public String getZonaHoraria(){
        return this.estadioAmostrar.getZonaHoraria();
    }

    public void setEstadioAMostrar(Estadio estadioAmostrar) {
        this.estadioAmostrar = estadioAmostrar;
    }
    public String getEstadioAMostrar(){
        return String.valueOf(estadioAmostrar);
    }



}
