package com.example.user30.entregamundialfutbol2018;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.user30.entregamundialfutbol2018.databinding.ActivityEquipoBinding;
import com.example.user30.entregamundialfutbol2018.model.Equipo;

public class EquipoActivity extends AppCompatActivity {

    private EquipoActivityController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new EquipoActivityController(this);
        ActivityEquipoBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_equipo);
        binding.setController(this.controller);

        Intent paseRecibido = this.getIntent();
        Equipo equipoAMostrar = (Equipo) paseRecibido.getSerializableExtra("equipo");
        controller.setEquipoAMostrar(equipoAMostrar);
    }

}
