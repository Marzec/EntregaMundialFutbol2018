package com.example.user30.entregamundialfutbol2018;

import android.databinding.BaseObservable;
import android.support.v7.app.AppCompatActivity;

import com.example.user30.entregamundialfutbol2018.model.Equipo;

public class EquipoActivityController extends BaseObservable {

    private AppCompatActivity activity;
    private Equipo equipoAmostrar;

    public EquipoActivityController(AppCompatActivity _activity) {
        this.activity = _activity;
    }

    public String getNombre() {
        return this.equipoAmostrar.getNombre();
    }
    public String getCodigo(){
        return this.equipoAmostrar.getCodigo();
    }
    public String getContinente(){
        return this.equipoAmostrar.getContinente();
    }

    public void setEquipoAMostrar(Equipo equipoAmostrar) {
        this.equipoAmostrar = equipoAmostrar;
    }
    public String getEquipoAMostrar(){
        return String.valueOf(equipoAmostrar);
    }



}


