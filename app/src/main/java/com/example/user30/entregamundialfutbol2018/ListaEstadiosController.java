package com.example.user30.entregamundialfutbol2018;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.user30.entregamundialfutbol2018.model.Estadio;
import com.example.user30.entregamundialfutbol2018.tools.ApplicationToolset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ListaEstadiosController extends BaseObservable {

    private AppCompatActivity activity;
    private List<Estadio> estadios = new ArrayList<>();
    private ArrayAdapter<Object> estadiosAdapterInicial;
    private SimpleAdapter segundoAdapter;


    public ListaEstadiosController(AppCompatActivity _activity, String estadioInicial) {
        super();
        this.activity = _activity;
        this.createEstadiosAdapterInicial();
        this.estadios=this.crearEstadio(estadioInicial);
        this.segundoAdapter = null;
        this.estadiosAdapterInicial.add("... cargando datos...");
        this.fetchEstadios();

    }

    @Bindable
    public BaseAdapter getEstadiosAdapter() {
        if (this.segundoAdapter == null) {
            return this.estadiosAdapterInicial;
        } else {
            return this.segundoAdapter;
        }
    }

    public void createEstadiosAdapterInicial() {
        this.estadiosAdapterInicial = new ArrayAdapter<>(
                this.activity, android.R.layout.simple_list_item_1
        );
    }

    public void createSegundoAdapter() {
        this.segundoAdapter = new SimpleAdapter(
                this.activity,
                this.crearMapasAMostrar(),
                android.R.layout.simple_list_item_2,
                new String[]{"nombre", "ciudad"},
                new int[]{android.R.id.text1, android.R.id.text2}
        );
        this.notifyPropertyChanged(BR.estadiosAdapter);
    }

    public List<Map<String, String>> crearMapasAMostrar() {
        ArrayList<Map<String, String>> losMapas = new ArrayList<>();
        for (Estadio estadio: estadios) {
            Map<String, String> infoEstadio = new HashMap<>();
            infoEstadio.put("nombre", estadio.getNombre());
            infoEstadio.put("ciudad", estadio.getCiudad());
            losMapas.add(infoEstadio);
        }
        return losMapas;
    }

    public void llenarEstadioAdapter() {
        this.estadiosAdapterInicial.clear();
        this.createSegundoAdapter();

    }

    public void fetchEstadios() {
        String url = "https://raw.githubusercontent.com/openfootball/world-cup.json/master/2018/worldcup.stadiums.json";

        JsonObjectRequest request = new JsonObjectRequest
                (
                        Request.Method.GET,
                        url,
                        null,
                        (JSONObject response) -> {
                            this.procesarRespuestaDelServer(response);
                            this.llenarEstadioAdapter();
                        },
                        (VolleyError error) -> {
                            error.printStackTrace();
                            throw new RuntimeException("Error en el request REST", error);
                        }
                );

        ApplicationToolset.toolset().addToRequestQueue(request);
    }

    //esto si estoy parada en el estadio stadiums
    public void procesarRespuestaDelServer(JSONObject respuesta) {
        try {
            JSONArray estadios=respuesta.getJSONArray("stadiums");
            for (int indice = 0; indice < estadios.length(); indice++) {
                JSONObject estadio = estadios.getJSONObject(indice);

                String nombre= estadio.getString("name");
                int capacidad=estadio.getInt("capacity");
                String ciudad=estadio.getString("city");
                String zonaHoraria=estadio.getString("timezone");


                this.estadios.add(new Estadio(nombre,capacidad,ciudad,zonaHoraria));
            }
        } catch (JSONException error) {
            error.printStackTrace();
            throw new RuntimeException("Error en el procesamiento del JSON", error);
        }
    }


//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    @Bindable
    public ListAdapter getEstadioAdapter() {
        ArrayAdapter<Estadio> adapter = new ArrayAdapter<>(
                this.activity, android.R.layout.simple_list_item_1
        );
        adapter.addAll(this.estadios);
        return adapter;
    }

    private List<Estadio> crearEstadio(String nombre) {
        List<Estadio> estadios = new ArrayList<>();
        for (Estadio estadio:estadios) {
            estadios.add(new Estadio(nombre));
        }
        return estadios;
    }

    public Estadio getEstadio(int position)  { return this.estadios.get(position); }

    public void mostrarEstadio(int position) {
        Intent saltoDeActivity = new Intent(this.activity, EstadioActivity.class);
        saltoDeActivity.putExtra("estadio", this.getEstadio(position));
        this.activity.startActivity(saltoDeActivity);
    }
}