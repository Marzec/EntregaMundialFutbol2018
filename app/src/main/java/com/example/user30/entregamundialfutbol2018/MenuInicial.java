package com.example.user30.entregamundialfutbol2018;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.user30.entregamundialfutbol2018.databinding.ActivityMenuInicialBinding;
import com.example.user30.entregamundialfutbol2018.tools.ApplicationToolset;


public class MenuInicial extends AppCompatActivity {

    MenuInicialController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ApplicationToolset.setContext(this.getApplicationContext());

        super.onCreate(savedInstanceState);
        controller = new MenuInicialController(this);
        ActivityMenuInicialBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_menu_inicial);
        binding.setController(this.controller);
    }
}