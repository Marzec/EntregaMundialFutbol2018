package com.example.user30.entregamundialfutbol2018.model;

import java.io.Serializable;

public class Equipo implements Serializable {

    private String nombre;
    private String codigo;
    private String continente;

    public Equipo(String nombre,String codigo,String continente){
        this.nombre=nombre;
        this.codigo=codigo;
        this.continente=continente;

    }
    public Equipo (String nombre){
        this.nombre=nombre;
    }

    public String getNombre() {
        return nombre;
    }
    public String getCodigo() {return codigo; }
    public String getContinente(){return continente;}

}
