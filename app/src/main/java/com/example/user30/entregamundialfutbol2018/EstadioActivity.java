package com.example.user30.entregamundialfutbol2018;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.user30.entregamundialfutbol2018.databinding.ActivityEstadioBinding;
import com.example.user30.entregamundialfutbol2018.model.Estadio;

public class EstadioActivity extends AppCompatActivity {

    private EstadioActivityController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new EstadioActivityController(this);
        ActivityEstadioBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_estadio);
        binding.setController(this.controller);

        Intent paseRecibido = this.getIntent();
        Estadio estadioAMostrar = (Estadio) paseRecibido.getSerializableExtra("estadio");
        controller.setEstadioAMostrar(estadioAMostrar);
    }

}
