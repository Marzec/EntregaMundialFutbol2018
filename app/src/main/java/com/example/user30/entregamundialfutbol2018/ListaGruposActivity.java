package com.example.user30.entregamundialfutbol2018;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.user30.entregamundialfutbol2018.databinding.ActivityListaGruposBinding;


public class ListaGruposActivity extends AppCompatActivity {
    private ListaGruposController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String grupoInicial = (String) this.getIntent().getSerializableExtra("groups");

        controller = new ListaGruposController(this,grupoInicial);
        ActivityListaGruposBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_lista_grupos);
        binding.setController(this.controller);

        this.agregarOnClick();
    }

    private void agregarOnClick() {
        ListView listaDeGrupos = this.findViewById(R.id.listaDeGrupos);

        listaDeGrupos.setOnItemClickListener((listView, viewFila, position, elIdDeNoSeQue) -> {
            this.controller.mostrarGrupo(position);
        });
    }

}
