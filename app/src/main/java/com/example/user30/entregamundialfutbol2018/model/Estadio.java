package com.example.user30.entregamundialfutbol2018.model;

import java.io.Serializable;

public class Estadio implements Serializable{
    protected String nombre;
    protected int capacidad;
    protected String ciudad;
    protected String zonaHoraria;

    public Estadio(String nombre,int capacidad,String ciudad,String zonaHoraria){
        this.nombre=nombre;
        this.capacidad=capacidad;
        this.ciudad=ciudad;
        this.zonaHoraria=zonaHoraria;
    }
    public Estadio(String nombre){
       this.nombre=nombre;
    }

    public String getCapacidad() {
        return String.valueOf(capacidad);
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getZonaHoraria() {
        return zonaHoraria;
    }
}
