package com.example.user30.entregamundialfutbol2018;

import android.databinding.BaseObservable;
import android.support.v7.app.AppCompatActivity;

import com.example.user30.entregamundialfutbol2018.model.Grupo;


public class GrupoActivityController extends BaseObservable{

    private AppCompatActivity activity;
    private Grupo grupoAmostrar;

    public GrupoActivityController(AppCompatActivity _activity) {
        this.activity = _activity;
    }

    public String getEquipo() {
        return this.grupoAmostrar.getEquipo();
    }
    public String getNombre(){
        return this.grupoAmostrar.getNombre();
    }

    public void setGrupoAMostrar(Grupo grupoAmostrar) {
        this.grupoAmostrar = grupoAmostrar;
    }
    public String getGrupoAMostrar(){
        return String.valueOf(grupoAmostrar);
    }



}
