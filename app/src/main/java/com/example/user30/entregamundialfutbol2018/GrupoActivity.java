package com.example.user30.entregamundialfutbol2018;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.user30.entregamundialfutbol2018.databinding.ActivityGrupoBinding;
import com.example.user30.entregamundialfutbol2018.model.Grupo;

public class GrupoActivity extends AppCompatActivity {

    private GrupoActivityController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new GrupoActivityController(this);
        ActivityGrupoBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_grupo);
        binding.setController(this.controller);

        Intent paseRecibido = this.getIntent();
        Grupo grupoAMostrar = (Grupo) paseRecibido.getSerializableExtra("grupo");
        controller.setGrupoAMostrar(grupoAMostrar);
    }


}
