package com.example.user30.entregamundialfutbol2018;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.user30.entregamundialfutbol2018.databinding.ActivityListaEstadiosBinding;

public class ListaEstadiosActivity extends AppCompatActivity {
    private ListaEstadiosController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String estadioInicial = (String) this.getIntent().getSerializableExtra("stadiums");

        controller = new ListaEstadiosController(this, estadioInicial);
        ActivityListaEstadiosBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_lista_estadios);
        binding.setController(this.controller);

        this.agregarOnClick();
    }

    private void agregarOnClick() {
        ListView listaDeEstadios = this.findViewById(R.id.listaDeEstadios);

        listaDeEstadios.setOnItemClickListener((listView, viewFila, position, elIdDeNoSeQue) -> {
            this.controller.mostrarEstadio(position);
        });
    }

}






