package com.example.user30.entregamundialfutbol2018;

import android.app.Application;

import com.jakewharton.threetenabp.AndroidThreeTen;

public class MundialApplication extends Application {
    @Override

    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
    }
}
