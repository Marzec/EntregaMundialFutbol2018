package com.example.user30.entregamundialfutbol2018;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.user30.entregamundialfutbol2018.model.Equipo;
import com.example.user30.entregamundialfutbol2018.tools.ApplicationToolset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListaEquiposController extends BaseObservable{


    private AppCompatActivity activity;
    private List<Equipo> equipos = new ArrayList<>();
    private ArrayAdapter<Object> equiposAdapterInicial;
    private SimpleAdapter segundoAdapter;


    public ListaEquiposController(AppCompatActivity _activity, String equipoInicial) {
        super();
        this.activity = _activity;
        this.createEquiposAdapterInicial();
        this.equipos=this.crearEquipo(equipoInicial);
        this.segundoAdapter = null;
        this.equiposAdapterInicial.add("... Espere, por favor..");
        this.fetchEquipos();

    }

    @Bindable
    public BaseAdapter getEquipoAdapter() {
        if (this.segundoAdapter == null) {
            return this.equiposAdapterInicial;
        } else {
            return this.segundoAdapter;
        }
    }

    public void createEquiposAdapterInicial() {
        this.equiposAdapterInicial = new ArrayAdapter<>(
                this.activity, android.R.layout.simple_list_item_1
        );
    }

    public void createSegundoAdapter() {
        this.segundoAdapter = new SimpleAdapter(
                this.activity,
                this.crearMapasAMostrar(),
                android.R.layout.simple_list_item_2,
                new String[]{"nombre","codigo"},
                new int[]{android.R.id.text1,android.R.id.text2}
        );
        this.notifyPropertyChanged(BR.equipoAdapter);
    }

    public List<Map<String, String>> crearMapasAMostrar() {
        ArrayList<Map<String, String>> losMapas = new ArrayList<>();
        for (Equipo equipo: equipos) {
            Map<String, String> infoEquipo = new HashMap<>();
            infoEquipo.put("nombre", equipo.getNombre());
            infoEquipo.put("codigo",equipo.getCodigo());
            losMapas.add(infoEquipo);
        }
        return losMapas;
    }

    public void llenarEquipoAdapter() {
        this.equiposAdapterInicial.clear();
        this.createSegundoAdapter();

    }

    public void fetchEquipos() {
        String url = "https://raw.githubusercontent.com/openfootball/world-cup.json/master/2018/worldcup.teams.json";

        JsonObjectRequest request = new JsonObjectRequest
                (
                        Request.Method.GET,
                        url,
                        null,
                        (JSONObject response) -> {
                            this.procesarRespuestaDelServer(response);
                            this.llenarEquipoAdapter();
                        },
                        (VolleyError error) -> {
                            error.printStackTrace();
                            throw new RuntimeException("Error en el request REST", error);
                        }
                );

        ApplicationToolset.toolset().addToRequestQueue(request);
    }


    public void procesarRespuestaDelServer(JSONObject respuesta) {
        try {
            JSONArray equipos=respuesta.getJSONArray("teams");
            for (int indice = 0; indice < equipos.length(); indice++) {
                JSONObject equipo = equipos.getJSONObject(indice);

                String nombre= equipo.getString("name");
                String codigo=equipo.getString("code");
                String continente=equipo.getString("continent");


                this.equipos.add(new Equipo(nombre,codigo,continente));
            }
        } catch (JSONException error) {
            error.printStackTrace();
            throw new RuntimeException("Error en el procesamiento del JSON", error);
        }
    }


//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


    private List<Equipo> crearEquipo(String nombre) {
        List<Equipo> equipos = new ArrayList<>();
        for (Equipo equipo: equipos) {
            equipos.add(new Equipo(nombre));
        }
        return equipos;
    }

    public Equipo getEquipo(int position)  { return this.equipos.get(position); }

    public void mostrarEquipo(int position) {
        Intent saltoDeActivity = new Intent(this.activity, EquipoActivity.class);
        saltoDeActivity.putExtra("equipo", this.getEquipo(position));
        this.activity.startActivity(saltoDeActivity);
    }

}
