package com.example.user30.entregamundialfutbol2018;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.support.v7.app.AppCompatActivity;

public class MenuInicialController extends BaseObservable {
    private AppCompatActivity activity;

    public MenuInicialController(AppCompatActivity activity){
        super();
        this.activity=activity;
    }
    public AppCompatActivity getActivity() {
        return activity;
    }


    public void mostrarEstadiosDeEquipos(){
        Intent salto = new Intent(this.activity, ListaEstadiosActivity.class);
        this.activity.startActivity(salto);
    }
    public void mostrarGruposDeEquipos(){
        Intent salto = new Intent(this.activity, ListaGruposActivity.class);
        this.activity.startActivity(salto);
    }
    public void mostrarEquipos(){
        Intent salto = new Intent(this.activity, ListaEquiposActivity.class);
        this.activity.startActivity(salto);
    }









}
