package com.example.user30.entregamundialfutbol2018;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.user30.entregamundialfutbol2018.model.Equipo;
import com.example.user30.entregamundialfutbol2018.model.Grupo;
import com.example.user30.entregamundialfutbol2018.tools.ApplicationToolset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ListaGruposController extends BaseObservable {

    private AppCompatActivity activity;
    private List<Grupo> grupos = new ArrayList<>();
    private ArrayAdapter<Object> gruposAdapterInicial;
    private SimpleAdapter segundoAdapter;

    public ListaGruposController(AppCompatActivity _activity,String grupoInicial) {
        super();
        this.activity = _activity;
        this.createGruposAdapterInicial();
        this.grupos=this.crearGrupo(grupoInicial);
        this.segundoAdapter = null;
        this.gruposAdapterInicial.add("...cargando datos ...");
        this.fetchGrupos();

    }

    @Bindable
    public BaseAdapter getGruposAdapter() {
        if (this.segundoAdapter == null) {
            return this.gruposAdapterInicial;
        } else {
            return this.segundoAdapter;
        }
    }

    public void createGruposAdapterInicial() {
        this.gruposAdapterInicial = new ArrayAdapter<>(
                this.activity, android.R.layout.simple_list_item_1
        );
    }
    public void createSegundoAdapter() {
        this.segundoAdapter = new SimpleAdapter(
                this.activity,
                this.crearMapasAMostrar(),
                android.R.layout.simple_list_item_2,
                new String[]{"nombre","equipo"},
                new int[]{android.R.id.text1,android.R.id.text2}
        );
        this.notifyPropertyChanged(BR.gruposAdapter);
    }

    public List<Map<String, String>> crearMapasAMostrar() {
        ArrayList<Map<String, String>> losMapas = new ArrayList<>();
        for (Grupo grupo: grupos) {
            Map<String, String> infoGrupo = new HashMap<>();
            infoGrupo.put("nombre", grupo.getNombre());
            infoGrupo.put("equipo", grupo.getEquipo());
            losMapas.add(infoGrupo);
        }
        return losMapas;
    }

    public void llenarGruposAdapter() {
        this.gruposAdapterInicial.clear();
        this.createSegundoAdapter();

    }

    public void fetchGrupos() {
        String url = "https://raw.githubusercontent.com/openfootball/world-cup.json/master/2018/worldcup.groups.json";

        JsonObjectRequest resquet = new JsonObjectRequest
                (
                        Request.Method.GET,
                        url,
                        null,
                        (JSONObject response) -> {
                            this.procesarRespuestaDelServer(response);
                            this.llenarGruposAdapter();
                        },
                        (VolleyError error) -> {
                            error.printStackTrace();
                            throw new RuntimeException("Error en el request REST", error);
                        }
                );

        ApplicationToolset.toolset().addToRequestQueue(resquet);
    }

    //esto si estoy parada en la lista de grupos
    public void procesarRespuestaDelServer(JSONObject respuesta) {
        try {
            JSONArray grupos= respuesta.getJSONArray("groups");
            for (int indice = 0; indice < grupos.length(); indice++) {
                JSONObject grupo= grupos.getJSONObject(indice);

                String nombre= grupo.getString("name");


                JSONArray equipos = grupo.getJSONArray("teams");

                this.grupos.add(new Grupo(nombre));

            }
        } catch (JSONException error) {
            error.printStackTrace();
            throw new RuntimeException("Error en el procesamiento del JSON", error);
        }
    }


    @Bindable
    public ListAdapter getGrupoAdapter() {
        ArrayAdapter<Grupo> adapter = new ArrayAdapter<>(
                this.activity, android.R.layout.simple_list_item_1
        );
        adapter.addAll(this.grupos);
        return adapter;
    }
    private List<Grupo> crearGrupo(String nombre) {
        List<Grupo> grupos = new ArrayList<>();
        for (Grupo grupo:grupos) {
            grupos.add(new Grupo(nombre));
        }
        return grupos;
    }

    public Grupo getGrupo(int position)  { return this.grupos.get(position); }

    public void mostrarGrupo(int position) {
        Intent saltoDeActivity = new Intent(this.activity, GrupoActivity.class);
        saltoDeActivity.putExtra("grupo", this.getGrupo(position));
        this.activity.startActivity(saltoDeActivity);
    }
}