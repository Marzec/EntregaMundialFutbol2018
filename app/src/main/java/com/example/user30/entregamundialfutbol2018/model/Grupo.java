package com.example.user30.entregamundialfutbol2018.model;

import java.io.Serializable;

public class Grupo implements Serializable{
    protected String nombre;
    protected String equipo;


public Grupo(String nombre,String equipo){
    this.nombre=nombre;
    this.equipo=equipo;
}

public Grupo(String nombre){
    this.nombre=nombre;
}

    public String getNombre() {
        return nombre;
    }

    public String getEquipo() {
        return equipo;
    }
}